# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project shows a method of designing a modular, programmable system to control automated warehouse equipment using Beckhoff's TwinCAT 3 development enviorment.  The goal of this project is to define an approach that can be adopted and used by a group of users familiar with object oriented programming methods to create, modify, test, and version source code.  All code development will use GIT as the source code control tool allowing multiple users to have access to the code.
* Version 1.0
* [Beckhoff PLC Project](https://infosys.beckhoff.com/content/1033/tc3_plc_intro/2526546571.html?id=6135567195050769302)

### How do I get set up? ###

* [Install TwinCAT 3 on PC](https://beckhoffautomation.com/english/download/tc3-download-xae.htm?id=1948695119487514)
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact