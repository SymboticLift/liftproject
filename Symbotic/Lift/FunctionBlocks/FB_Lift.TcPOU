﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.7">
  <POU Name="FB_Lift" Id="{362d2240-6752-4e94-8ee1-9c3377440df7}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK PUBLIC FB_Lift EXTENDS FB_Controller IMPLEMENTS ILift

VAR PERSISTENT
	stXmlLiftData		: ST_XML_LIFT;		// Retain Actual Lift XML Data
END_VAR

VAR
	xInitialize			: BOOL		:= FALSE;// Initialize Read of XML Data
	FB_MastAxis			: FB_AxisVertical;	// Axis control for main mast axis
	FB_LHDAxis			: FB_Axis;			// Axis control for load handling device
	FB_XmlOop			: FB_XmlPosition;	// XML Read And Write Function Block
	FB_LHDCaseDetector	: FB_DigitalInput;	// Part detection for load handling device
	astHmiMessageQue	: ARRAY [0..GC.MAX_MESSAGE_QUE_SIZE] OF ST_Message;
	uiMessagesInQue		: UINT;				// Messagee Read From Super Control
	iAutoState			: INT;				// State machine index
	stMessage			: ST_Message;		// Current message Data Struct
	stResponseMsg		: ST_Message;		// Response message
	tonStarted			: TON;				// Started Timer
	
	sUpdateFolder		: STRING;			// Temp variable used to verify the update folder information is passed into the controller
	iFaultState			: INT;				// Curent fault state 
	uiMovePosBaseline	: UINT;				// Monitor Last Move Position ID
	
	stHmiData			: ST_HMI;			// HMI Data
	
	uiPortNumber		: UINT;				// Lift instancs Port Number
	
	lrSetVelocityMast	: LREAL;			// Velocity Settings
	lrSetVelocityLhd	: LREAL;			// Velocity Settings

	uiIndex 			: UINT;				// HMI For Loop Index in HMI Action
	
	xMovePositionStart	: BOOL;				// Execute Move Command
	xMovePosition		: BOOL;				// Move Complete Trigger
	xPickStart			: BOOL;				// Execute Pick Command
	xPick				: BOOL;				// Pick Complete Trigger
	xPlaceStart			: BOOL;				// Execute Place Command
	xPlace				: BOOL;				// Place Complete Trigger
	
	uiMotionCase		: INT;				// Move, Place, And Pick State Machine Index
	
	stEvent				: ST_Event;			// Event Structure
	uiSetPositionID		: UINT;				// Position ID Set From The FB Move Methods
	
	tmrLiftWatchdog		: TON;				// Watchdog Timer
			
	rtMachineMode		: R_TRIG;			// OneShot to force Machine Mode to Diagnostic if Pack ML Not in Execute
	
//------------------------------------SAFETY------------------------------------------	
	I_xSafeyNoSTO			AT %I*	: BOOL	:= TRUE;
	I_xSafetyCOMerr			AT %I*	: BOOL;
	I_xSafetyFBerr			AT %I*	: BOOL;
	Q_xSafetyAck			AT %Q*	: BOOL;
	Q_xSafetyRestart		AT %Q*	: BOOL;
	Q_xSafetyStartStop		AT %Q*	: BOOL	:=TRUE;
	
	rtSafeOut			: R_TRIG;
//------------------------------------------------------------------------------------
	tmrStartDelay		: TON;
	eTest				: E_State;
	uiHmiAlarmIndex		: INT;
//	iIndex				: INT;
	FB_StartProcess		: NT_StartProcess;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[
//-------------------CALL INSTANCE OF FB_CONTROLLER----------------------------
SUPER^();

CASE PackState OF
	E_State.Starting:
		//Enabling the drives
		IF FB_MastAxis.AxisState= E_State.Execute AND FB_LHDAxis.AxisState = E_State.Execute THEN
			PackState := E_State.Execute;
		END_IF
	
	E_State.Execute:
		IF MachineMode = E_MachineMode.Manual OR MachineMode = E_MachineMode.Maintenance THEN
			
			FB_MastAxis.JogFwd := stHmiData.xJogMastUp;
			FB_MastAxis.JogBwd := stHmiData.xJogMastDown;
			FB_LHDAxis.JogFwd := stHmiData.xJogLHDFwd;
			FB_LHDAxis.JogBwd := stHmiData.xJogLHDBwd;
		
			IF stHmiData.xHomeLHD THEN
				FB_LHDAxis.HomeAxis();
				stHmiData.xHomeLHD	:= FALSE;
			END_IF
		
			IF FB_LHDAxis.Homed THEN		//	LHD AXIS MUST BE HOMED FIRST
				IF stHmiData.xHomeMast THEN
					FB_MastAxis.HomeAxis();
					stHmiData.xHomeMast	:= FALSE;
				END_IF
			END_IF
		ELSE
			FB_MastAxis.JogFwd := FALSE;
			FB_MastAxis.JogBwd := FALSE;
			FB_LHDAxis.JogFwd := FALSE;
			FB_LHDAxis.JogBwd := FALSE;
		END_IF
		
	E_State.Stopping:			// The supervisory control sent a stop command
		IF FB_MastAxis.AxisState = E_State.Stopped AND FB_LHDAxis.AxisState = E_State.Stopped THEN
			PackState := E_State.Stopped;
		END_IF
		fbInputMessageQue.Clear();
		
	E_State.Aborting:	
		IF FB_MastAxis.AxisState = E_State.Aborted AND FB_LHDAxis.AxisState = E_State.Aborted THEN
			PackState := E_State.Aborted;		// Change the PackML state to aborted
		END_IF
		
	E_State.Aborted:
		IF I_xSafeyNoSTO THEN //Safety system is ready
			PackState := E_State.Stopped;
		END_IF
		
	E_State.Stopped:
		//Waiting for a reset command from supervisory control
	
	E_State.Resetting:
		IF FB_MastAxis.AxisState = E_State.Idle AND FB_LHDAxis.AxisState = E_State.Idle THEN
			PackState := E_State.Idle;
		END_IF
		
	E_State.Idle:
		//Waiting for a start command	
		
END_CASE

//-----------------------------------------------------------------------------
eTest := PackState;

//------------------------SET DATA FOR LIFT------------------------------------
Name := CONCAT('Lift',UINT_TO_STRING(uiPortNumber));
FB_MastAxis.Name := CONCAT('Mast Axis',UINT_TO_STRING(uiPortNumber));
FB_LHDAxis.Name := CONCAT('LHD Axis',UINT_TO_STRING(uiPortNumber));

FB_XmlOop();
FB_LHDCaseDetector();
//-----------------------------------------------------------------------------

//--------SAFTEY CIRCUIT SETS LIFT TO DIAGNOSTIC AND AXES TO ABORTING----------
rtSafeOut(CLK:=NOT(I_xSafeyNoSTO));
	IF rtSafeOut.Q THEN
		MachineMode := E_MachineMode.Diagnostic;	
		stEvent.diEventID := UDINT_TO_DINT(14);												// Set the error number "E-Stop"
		stResponseMsg.sMessage := GC.ALARM_STATUScmd;
		fbOutputMessageQue.AddMessage(stResponseMsg);
		stEvent.eEventSource := E_EventSource.User;											// Set the source of the event
		stEvent.eEventType := E_EventType.Error;											// Set the error level for the event
		stEvent.sDescription := CONCAT(CONCAT(THIS^.Name,' - '),F_GetErrorText(stEvent));	// Get the text value for the error number
		SetEvent(stEvent);																	// Send event to event handler
	END_IF
//-----------------------------------------------------------------------------------

//---------------------E-STOP OR AXIS REQUEST ABORTING--------------------------------
IF FB_MastAxis.AxisState = E_State.Aborting OR FB_LHDAxis.AxisState = E_State.Aborting OR rtSafeOut.Q THEN
	PackState := E_State.Aborting;
	// Handle stopping all axis if there is an e-stop or one of the axis has faulted
END_IF
//-----------------------------------------------------------------------------

//------------------CALL AXIS FUNCTION BLOCK INSTANCES-------------------------
//FB_MastAxis(e_PackState := PackState, e_MachineMode:= MachineMode, lrStopDecel := stXmlLiftData.lrStopDecel, lrStopJerk := stXmlLiftData.lrStopJerk);
//FB_LHDAxis(e_PackState := PackState, e_MachineMode:= MachineMode, lrStopDecel := stXmlLiftData.lrStopDecel, lrStopJerk := stXmlLiftData.lrStopJerk);
FB_MastAxis();
FB_LHDAxis();
stHmiData.eLiftState := PackState;
//-----------------------------------------------------------------------------



//--------------------PLC INIT AND MANUAL XML READ-----------------------------
IF NOT(xInitialize) OR stHmiData.xReadXmlData THEN
	FB_XmlOop.ElementPath := CONCAT(GC.XML_PATH_LOCATION,'\Lift');
	FB_XmlOop.ElementPath := CONCAT(FB_XmlOop.ElementPath,UINT_TO_STRING(PortNumber));
	FB_XmlOop.ElementPath := CONCAT(FB_XmlOop.ElementPath,'.xml');
	
	FB_XmlOop.ElementName := '/Lift';
	FB_XmlOop.Read();
	stXmlLiftData := FB_XmlOop.ElementData;
	stHmiData.xReadXmlData := FALSE;
	
	tmrStartDelay(IN := NOT(tmrStartDelay.Q), PT := T#5S);
	IF tmrStartDelay.Q THEN
		xInitialize := TRUE;	
	END_IF
END_IF
//-----------------------------------------------------------------------------


//------------------------MANUAL XML WRITE-------------------------------------
IF stHmiData.xWriteXmlData THEN
	FB_XmlOop.ElementPath := CONCAT(GC.XML_PATH_LOCATION,'\Lift');
	FB_XmlOop.ElementPath := CONCAT(FB_XmlOop.ElementPath,UINT_TO_STRING(PortNumber));
	FB_XmlOop.ElementPath := CONCAT(FB_XmlOop.ElementPath,'.xml');
	
	FB_XmlOop.ElementName := '/Lift';
	FB_XmlOop.ElementData := stXmlLiftData;
	FB_XmlOop.Write();
	stHmiData.xWriteXmlData := FALSE;
END_IF
//-----------------------------------------------------------------------------
 

//----------------------------HMI SETS LIFTS MACHINE STATE----------------------
IF stHmiData.xNavAutomatic THEN
	stHmiData.eMachineState := E_MachineMode.Auto;
ELSIF stHmiData.xNavManual THEN
	stHmiData.eMachineState := E_MachineMode.Manual;
ELSIF stHmiData.xNavManitenance THEN
	stHmiData.eMachineState := E_MachineMode.Maintenance;
ELSIF stHmiData.xNavDiagnostic THEN
	stHmiData.eMachineState := E_MachineMode.Diagnostic;
END_IF
//-----------------------------------------------------------------------------


//-----------------------------COMMAND TYPE--------------------------------------
stResponseMsg.eCommandType := E_MessageType.Response;


//----------------------LIFT CONTROL STATE MACHINE-------------------------------
CASE iAutoState OF	
//-------------------------------INITIALIZE--------------------------------------
	0:	iAutoState		:= 10;
		
//------------------------LOOK FOR MESSAGE IN QUE--------------------------------
	10: tmrLiftWatchdog(IN := FALSE);
		xMovePosition	:= FALSE;
		xPick			:= FALSE;
		xPlace			:= FALSE;
		IF THIS^.fbInputMessageQue.MessagesInQue > 0 THEN
			stResponseMsg.sMsgID := stMessage.sMsgID;
			stResponseMsg.sMessage := stMessage.sMessage;
			stMessage := THIS^.fbInputMessageQue.GetMessage();
			IF stMessage.eCommandType = E_MessageType.ActionCommand THEN
				iAutoState := 20;		// Handle Action command
			ELSIF stMessage.eCommandType = E_MessageTYpe.PackMLCommand THEN
				iAutoState := 100;		// Handle PachMS command
			ELSIF stMessage.eCommandType = E_MessageType.UpdateCommand THEN
				iAutoState := 200;		// Handle updating the controller
			END_IF
		END_IF
		IF PackState = E_State.Stopped AND stHmiData.xUpdate THEN
			iAutoState := 200;		// Handle updating the controller
		END_IF
		
//------------------------GENERATE STARTED RESPONSE COMMAND-------------------------
	20:	IF stMessage.eCommandType = E_MessageType.ActionCommand THEN
			stResponseMsg.sMessage := GC.STARTEDcmd;
			stResponseMsg.sMsgID := stMessage.sMsgID;
			THIS^.fbOutputMessageQue.AddMessage(stResponseMsg);
			iAutoState := 30;
		END_IF
		IF stMessage.eCommandType = E_MessageType.UpdateCommand THEN
			stResponseMsg.sMessage := GC.STARTEDcmd;
			stResponseMsg.sMsgID := stMessage.sMsgID;
			THIS^.fbOutputMessageQue.AddMessage(stResponseMsg);
			iAutoState := 10000;
		END_IF
		
//------------------------EXECUTE MOTION ACTIONS----------------------------------
	30:	
//---------------------------EXECUTE MOVE-----------------------------------------
		IF stMessage.sMessage = GC.MOVEcmd THEN							// If Move Type Move Command
			IF stHmiData.astLevelData[stMessage.uiPositionID].xEnabled THEN		// Check if Level XML Position is enabled(Position <> Zero
				IF 	stMessage.eMoveType = E_MoveType.Pick THEN				// If Action Will Be A Pick
					xMovePositionStart := MovePosition(Position:=stMessage.uiPositionID);	//Call Method and Set Position ID Value
					IF xMovePositionStart THEN
						uiMovePosBaseline := stMessage.uiPositionID;
						tmrLiftWatchdog(IN := FALSE);
						xMovePositionStart := FALSE;
						iAutoState := 2000;
					ELSE
						iAutoState := 1000;						
					END_IF
				ELSIF stMessage.eMoveType = E_MoveType.Place THEN
					xMovePositionStart := MovePosition(Position:=stMessage.uiPositionID);
					IF xMovePositionStart THEN
						uiMovePosBaseline := stMessage.uiPositionID;
						tmrLiftWatchdog(IN := FALSE);
						xMovePositionStart := FALSE;
						iAutoState := 3000;
					ELSE
						iAutoState := 1000;	
					END_IF
				ELSE
					stEvent.diEventID := UDINT_TO_DINT(13);	// Set the error number "Move Type Not Defined"
					iAutoState := 1000;					
				END_IF
			ELSE
				stEvent.diEventID := UDINT_TO_DINT(17);		// Set the error number "Level Disabled"
				iAutoState := 1000;
			END_IF
//---------------------------EXECUTE PICK------------------------------------------	
		ELSIF stMessage.sMessage = GC.PICKcmd THEN
			xPickStart := Pick();
			IF xPickStart THEN
				tmrLiftWatchdog(IN := FALSE);
				xPickStart := FALSE;
				iAutoState := 4000;
			ELSE
				iAutoState := 1000;	
			END_IF
//----------------------------------------------------------------------------------

//-------------------------EXECUTE PLACE--------------------------------------------								
		ELSIF stMessage.sMessage = GC.PLACEcmd THEN			
			xPlaceStart := Place();
			IF xPlaceStart THEN
				tmrLiftWatchdog(IN := FALSE);
				xPlaceStart := FALSE;
				iAutoState := 5000;
			ELSE
				iAutoState := 1000;	
			END_IF
		END_IF
//-----------------------------------------------------------------------------------	

//--------------------------GENERATE COMPLETED COMMAND-------------------------------		
	40:	stResponseMsg.sMessage := GC.COMPLETEcmd;
		THIS^.fbOutputMessageQue.AddMessage(stResponseMsg);
		iAutoState := 10;

//--------------------------GENERATE ACK RESPONSE------------------------------------
	50:
		stResponseMsg.sMessage := stMessage.sMessage;
		stResponseMsg.sAckNakResponse := GC.ACK;
		THIS^.fbOutputMessageQue.AddMessage(stResponseMsg);
		iAutoState := 10;
		
//--------------------------GENERATE NAK RESPONSE------------------------------------
	60:
		stResponseMsg.sMessage := stMessage.sMessage;
		stResponseMsg.sAckNakResponse := GC.NAK;
		THIS^.fbOutputMessageQue.AddMessage(stResponseMsg);
		iAutoState := 10;
		
//--------------------------Handle PackML command------------------------------------
	100:
		IF SUPER^.MachineMode = E_MachineMode.Auto THEN	
			iAutoState := 110;		
			IF stMessage.sMessage = GC.STARTcmd AND PackState = E_State.Idle THEN			// Supervisory is requesting a start
				PackState := E_State.Starting;												// System is in the correct state to switch to Start
			ELSIF stMessage.sMessage = GC.STOPcmd AND PackState = E_State.Execute THEN		// Supervisory is requesting a stop
				PackState := E_State.Stopping;												// System is in the correct state to be able to switch into a Stopped state
			ELSIF stMessage.sMessage = GC.RESETcmd AND PackState = E_State.Stopped THEN		// Supervisory is requesting a reset
				PackState := E_State.Resetting;												// System is in the correct state to be able to swith into an idle state
			ELSE 
				iAutoState := 60;															// Not in the correct state to be able to change to that PackML state
			END_IF
		ELSE
			iAutoState := 60;																// Send a Nack back to supervisory control if the lift is not in auto mode
		END_IF
		
	110:
		tmrLiftWatchdog(IN := TRUE, PT := T#1S);	// Allow the controller time to switch to the requested state
		IF PackState = E_State.Stopped OR PackState = E_State.Execute OR PackState = E_State.Idle THEN
			iAutoState := 50;	// ACK
		END_IF
		IF tmrLiftWatchdog.Q THEN
			iAutoState := 60;	// NAK
		END_IF
	
		
//--------------------------UPDATE THE CONTROLLER------------------------------------
	200:
		//sUpdateFolder := stMessage.sUpdateSourceFolder;
		FB_StartProcess(PATHSTR:='C:\TwinCAT\3.1\BootUpdate\LoadBoot.bat',
				DIRNAME:='C:\TwinCAT\3.1\BootUpdate',
				START:=TRUE,
				TMOUT:=T#30S ); 

		iAutoState := 10;		

		
//------------------------------------ERROR-------------------------------------------		
	1000:	tmrLiftWatchdog(IN := FALSE);
			stResponseMsg.sMessage := GC.FAILEDcmd;
			fbOutputMessageQue.AddMessage(stResponseMsg);
			stEvent.eEventSource := E_EventSource.User;			// Set the source of the event
			stEvent.eEventType := E_EventType.Error;			// Set the error level for the event
			stEvent.sDescription := CONCAT(CONCAT(THIS^.Name,' - '),F_GetErrorText(stEvent));	// Get the text value for the error number													// Error
			SetEvent(stEvent);							// Send event to event handler
			iAutoState := 10;
//------------------------------------------------------------------------------------

//---------------------------------------MOVE TO PICK ACTION ---------------------------------		
	2000:	tmrLiftWatchdog(IN := (FB_MastAxis.AxisState <> E_STATE.Execute), PT := T#10S);
			IF tmrLiftWatchdog.Q THEN
				uiMotionCase := 1000;
			END_IF
			
//-------------------------------MOVE TO PICK ACTION STATE MACHINE-----------------------------			
			CASE uiMotionCase OF
				0:	xMovePosition := FALSE;
					uiMotionCase := 10;
					
				10: IF FB_LhdAxis.CurrentPosition - 5 < 0 AND FB_LhdAxis.CurrentPosition + 5 > 0 THEN
						FB_MastAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity01, AccelDecel := stXmlLiftData.lrAccelDecel01, Jerk := stXmlLiftData.lrJerk01, Position := stXmlLiftData.lrLevel[uiSetPositionID]-stXmlLiftData.lrOffset);
						IF FB_MastAxis.AxisState = E_State.Execute THEN
							uiMotionCase := 20;
						END_IF
					ELSE
						stEvent.diEventID := UDINT_TO_DINT(15);		// Set the error number "Horizontal Axis is Extended"
						uiMotionCase	:= 0;
						iAutoState	:= 1000;	
					END_IF
					
				20: IF FB_MastAxis.AxisState = E_State.Complete THEN
						xMovePosition := TRUE;
						IF xMovePosition THEN
							xMovePosition := FALSE;
							uiMotionCase := 0;
							iAutoState		 := 40;
						END_IF
					END_IF
							
				1000:	tmrLiftWatchdog(IN :=  FALSE);
						xMovePosition := FALSE;
						uiMotionCase	:= 0;						
						stEvent.diEventID := UDINT_TO_DINT(4);		// Set the error number "Move Command Failed"
						iAutoState		:= 1000;
			END_CASE
//-------------------------------------------------------------------------------------	

//---------------------------------------MOVE TO PLACE ACTION ---------------------------------		
	3000:	tmrLiftWatchdog(IN := (FB_MastAxis.AxisState <> E_STATE.Execute), PT := T#10S);
			IF tmrLiftWatchdog.Q THEN
				uiMotionCase := 1000;
			END_IF
			
//-------------------------------MOVE TO PLACE ACTION STATE MACHINE-----------------------------			
			CASE uiMotionCase OF
				0:	xMovePosition := FALSE;
					uiMotionCase := 10;
					
				10: IF FB_LhdAxis.CurrentPosition - 5 < 0 AND FB_LhdAxis.CurrentPosition + 5 > 0 THEN
						FB_MastAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity01, AccelDecel := stXmlLiftData.lrAccelDecel01, Jerk := stXmlLiftData.lrJerk01, Position := stXmlLiftData.lrLevel[uiSetPositionID]+stXmlLiftData.lrOffset);
						IF FB_MastAxis.AxisState = E_State.Execute THEN
							uiMotionCase := 20;
						END_IF
					ELSE
						stEvent.diEventID := UDINT_TO_DINT(15);		// Set the error number "Horizontal Axis is Extended"
						uiMotionCase	:= 0;
						iAutoState	:= 1000;	
					END_IF
				
				20: IF FB_MastAxis.AxisState = E_State.Complete THEN
						xMovePosition := TRUE;
						IF xMovePosition THEN
							xMovePosition := FALSE;
							uiMotionCase := 0;
							iAutoState		 := 40;
						END_IF
					END_IF
							
				1000:	tmrLiftWatchdog(IN :=  FALSE);
						xMovePosition := FALSE;
						uiMotionCase	:= 0;						
						stEvent.diEventID := UDINT_TO_DINT(4);		// Set the error number "Move Command Failed"
						iAutoState		:= 1000;
			END_CASE
//-------------------------------------------------------------------------------------

//-------------------------------PICK ACTION STATE MACHINE-----------------------------
	4000:	tmrLiftWatchdog(IN := (FB_MastAxis.AxisState <> E_STATE.Execute) AND (FB_LHDAxis.AxisState <> E_STATE.Execute), PT := T#10S);
			IF tmrLiftWatchdog.Q THEN
				uiMotionCase := 1000;
			END_IF
		
			CASE uiMotionCase OF
				0:		xPick := FALSE;
						uiMotionCase := 40;
			
				40:		FB_LHDAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity02, AccelDecel := stXmlLiftData.lrAccelDecel02, Jerk := stXmlLiftData.lrJerk02, Position := stXmlLiftData.lrLHDExtend);
						IF FB_LHDAxis.AxisState = E_State.Execute THEN
							uiMotionCase := 60;
						END_IF
					
				60:		IF FB_LHDAxis.AxisState = E_State.Complete THEN
							uiMotionCase := 70;
						END_IF		
					
				70:		IF FB_LhdAxis.CurrentPosition - 5 < stXmlLiftData.lrLHDExtend AND FB_LhdAxis.CurrentPosition + 5 > stXmlLiftData.lrLHDExtend THEN
							FB_MastAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity01, AccelDecel := stXmlLiftData.lrAccelDecel01, Jerk := stXmlLiftData.lrJerk01, Position := stXmlLiftData.lrOffset + stXmlLiftData.lrLevel[uiSetPositionID]);
							IF FB_MastAxis.AxisState = E_State.Execute THEN
								uiMotionCase := 90;
							END_IF
						ELSE
							stEvent.diEventID := UDINT_TO_DINT(16);		// Set the error number "Horizontal Axis is NOT Extended"
							uiMotionCase	:= 0;
							iAutoState	:= 1000;	
						END_IF
					
				90:		IF FB_MastAxis.AxisState = E_State.Complete THEN
							uiMotionCase := 100;
						END_IF		
			
				100:	FB_LHDAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity02, AccelDecel := stXmlLiftData.lrAccelDecel02, Jerk := stXmlLiftData.lrJerk02, Position := 0);
						IF FB_LHDAxis.AxisState = E_State.Execute THEN
							uiMotionCase := 120;
						END_IF
					
				120:	IF FB_LHDAxis.AxisState = E_State.Complete THEN
							xPick := TRUE;
							IF xPick THEN
								xPick := FALSE;
								uiMotionCase := 0;
								iAutoState		 := 40;
							END_IF
						END_IF
					 
				1000:	tmrLiftWatchdog(IN :=  FALSE);
					 	xPick := FALSE;
						uiMotionCase	:= 0;						
						stEvent.diEventID := UDINT_TO_DINT(5);		// Set the error number "Pick Command Failed"
						iAutoState		:= 1000;			
			END_CASE
//--------------------------------------------------------------------------------------

//-------------------------------PLACE ACTION STATE MACHINE-----------------------------	
	5000:	tmrLiftWatchdog(IN := (FB_MastAxis.AxisState <> E_STATE.Execute) AND (FB_LHDAxis.AxisState <> E_STATE.Execute), PT := T#10S);
			IF tmrLiftWatchdog.Q THEN
				uiMotionCase := 1000;
			END_IF
			
			CASE uiMotionCase OF
				0:	xPlace := FALSE;
					uiMotionCase := 40;
			
				40:	FB_LHDAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity02, AccelDecel := stXmlLiftData.lrAccelDecel02, Jerk := stXmlLiftData.lrJerk02, Position := stXmlLiftData.lrLHDExtend);
					IF FB_LHDAxis.AxisState = E_State.Execute THEN
						uiMotionCase := 60;
					END_IF
					
				60: IF FB_LHDAxis.AxisState = E_State.Complete THEN
						uiMotionCase := 70;
					END_IF		
					
				70: IF FB_LhdAxis.CurrentPosition - 5 < stXmlLiftData.lrLHDExtend AND FB_LhdAxis.CurrentPosition + 5 > stXmlLiftData.lrLHDExtend THEN
						FB_MastAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity01, AccelDecel := stXmlLiftData.lrAccelDecel01, Jerk := stXmlLiftData.lrJerk01, Position := -stXmlLiftData.lrOffset + stXmlLiftData.lrLevel[uiSetPositionID]);
						IF FB_MastAxis.AxisState = E_State.Execute THEN
							uiMotionCase := 90;
						END_IF
					ELSE
						stEvent.diEventID := UDINT_TO_DINT(16);		// Set the error number "Horizontal Axis is NOT Extended"
						uiMotionCase	:= 0;
						iAutoState	:= 1000;	
					END_IF
					
				90: IF FB_MastAxis.AxisState = E_State.Complete THEN
						uiMotionCase := 100;
					END_IF		
			
				100: FB_LHDAxis.MoveAbsolute(Velocity := stXmlLiftData.lrVelocity02, AccelDecel := stXmlLiftData.lrAccelDecel02, Jerk := stXmlLiftData.lrJerk02, Position := 0);
					 IF FB_LHDAxis.AxisState = E_State.Execute THEN
						uiMotionCase := 120;
					 END_IF
					
				120: IF FB_LHDAxis.AxisState = E_State.Complete THEN
						xPlace := TRUE;
						IF xPlace THEN
							xPlace := FALSE;
							uiMotionCase := 0;
							iAutoState		 := 40;
						END_IF
					 END_IF
		
				1000:	tmrLiftWatchdog(IN :=  FALSE);
						xPlace := FALSE;
						uiMotionCase	:= 0;						
						stEvent.diEventID := UDINT_TO_DINT(6);		// Set the error number "Place Command Failed"
						iAutoState		:= 1000;
			END_CASE
//-----------------------------------------------------------------------------------------	
END_CASE

//-------------------------------MESSAGE IN QUE--------------------------------------------
uiMessagesInQue := fbInputMessageQue.MessagesInQue;

//-------------------------------CALL HMI LIFT DATA ACTION---------------------------------
HMI();	// CALL HMI ACTION
//--------------------------
]]></ST>
    </Implementation>
    <Method Name="FB_init" Id="{86a9dda4-3971-4e82-82b1-41995f9e682f}">
      <Declaration><![CDATA[METHOD FB_init : BOOL
VAR_INPUT
	bInitRetains : BOOL; // if TRUE, the retain variables are initialized (warm start / cold start)
	bInCopyCode : BOOL;  // if TRUE, the instance afterwards gets moved into the copy code (online change)
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[FB_MastAxis.PackstatePt:=PackStatePt;
FB_MastAxis.MachineModePt:=MachineModePt;
FB_MastAxis.StopDecelPt:=ADR(stXmlLiftData.lrStopDecel);
FB_MastAxis.StopJerkPt:=ADR(stXmlLiftData.lrStopJerk);
FB_LHDAxis.PackstatePt:=PackStatePt;
FB_LHDAxis.MachineModePt:=MachineModePt;
FB_LHDAxis.StopDecelPt:=ADR(stXmlLiftData.lrStopDecel);
FB_LHDAxis.StopJerkPt:=ADR(stXmlLiftData.lrStopJerk);
]]></ST>
      </Implementation>
    </Method>
    <Action Name="HMI" Id="{a3da16dd-d430-4aad-ba0f-4919b063cc6b}">
      <Implementation>
        <ST><![CDATA[
//-----------COUNT LEVEL QTY, ENABLE LEVEL, SET TOP PIXEL VALUE-------------
stHmiData.uiLevelQty := 0;																		// Set To Zero To Account For Changes To XML File
stHmiData.astLevelData[0].xEnabled := TRUE;
FOR uiIndex := 1 TO GC.uiMAX_LEVEL_QTY DO
	IF stXmlLiftData.lrLevel[uiIndex] <> 0 THEN													// LEVEL <> = Then Enable LEVEL And Add To Active Quantity Count
		stHmiData.astLevelData[uiIndex].xEnabled := TRUE;
		stHmiData.uiLevelQty := stHmiData.uiLevelQty +1;
	ELSE
		stHmiData.astLevelData[uiIndex].xEnabled := FALSE;										// lEVEL = 0 Then Disable LEVEL
	END_IF
	
	IF stXmlLiftData.lrLevel[uiIndex] < stXmlLiftData.lrMastExtend THEN
		stHmiData.astLevelData[uiIndex].lrTopPx := ABS((1000*stXmlLiftData.lrLevel[uiIndex])/(stXmlLiftData.lrMastExtend+0.001)-1000)-30; //Scale Y Axis lEVELS Based On Actual -30px offset For Top Justified PX
	END_IF
END_FOR
//--------------------------------------------------------------------------


//----------------SET LEVEL SCALE FOR RESIZING LEVEL HEIGHTS----------------
IF stHmiData.uiLevelQty <= 5 THEN
	stHmiData.lrLevelScale := 1000;																	// Level Pixel Scale Standard/Unchanged = 1000
ELSE
	stHmiData.lrLevelScale := ABS(((UINT_TO_LREAL(stHmiData.uiLevelQty)-5)*50)-1000);  				// Level 250px(Smallest) To 1000px(Standard) Scale
END_IF
//--------------------------------------------------------------------------


//----------------SET LIFT MAST VERTICAL PIXEL POSITION SCALE---------------
IF FB_MastAxis.CurrentPosition < stXmlLiftData.lrMastExtend THEN
	stHmiData.lrVertPos := ABS((1000*FB_MastAxis.CurrentPosition/(stXmlLiftData.lrMastExtend+0.001))-1000)-30;  //Scale Y Axis POSITION Based On Actual -30px Offset For Top Justified PX
END_IF
//--------------------------------------------------------------------------


//----------------SET LIFT LHD HORIZ PIXEL POSITION SCALE-------------------
IF FB_LHDAxis.CurrentPosition < stXmlLiftData.lrLHDExtend THEN
	stHmiData.lrHorizPos := ABS((150*FB_LHDAxis.CurrentPosition/(stXmlLiftData.lrLHDExtend+0.001))-150);		//Scale X Axis POSITION Based On Actual
END_IF
//---------------------------------------------------------------------------


//-----------------GET MESSAGE QUE FOR HMI AUTO DISPLAY----------------------
IF 	eMachineMode = E_MachineMode.Auto THEN
	astHmiMessageQue := fbInputMessageQue.Events;
END_IF
//---------------------------------------------------------------------------


//-----------------ALARM TABLE DISPLAY----------------------
IF stHmiData.xAlarmNext THEN
	IF stHmiData.uiAlarmInstanceIndex < 90 THEN
		stHmiData.uiAlarmInstanceIndex	:= stHmiData.uiAlarmInstanceIndex + 1;
	END_IF
	stHmiData.xAlarmNext := FALSE;
END_IF

IF stHmiData.xAlarmPrevious THEN
	IF stHmiData.uiAlarmInstanceIndex > 0 THEN
		stHmiData.uiAlarmInstanceIndex	:= stHmiData.uiAlarmInstanceIndex - 1;
	END_IF
	stHmiData.xAlarmPrevious := FALSE;
END_IF

IF stHmiData.xPageNext THEN
	IF stHmiData.uiAlarmInstanceIndex < 90 THEN
		stHmiData.uiAlarmInstanceIndex	:= stHmiData.uiAlarmInstanceIndex + 10;
	END_IF
	stHmiData.xPageNext := FALSE;
END_IF

IF stHmiData.xPagePrevious THEN
	IF stHmiData.uiAlarmInstanceIndex > 0 THEN
		stHmiData.uiAlarmInstanceIndex	:= stHmiData.uiAlarmInstanceIndex - 10;
	END_IF
	stHmiData.xPagePrevious := FALSE;
END_IF

FOR uiHmiAlarmIndex := 1 TO 10 DO
	stHmiData.astDisplayEvents[uiHmiAlarmIndex] := arEvents[(uiHmiAlarmIndex)+stHmiData.uiAlarmInstanceIndex];
END_FOR

//---------------------------------------------------------------------------]]></ST>
      </Implementation>
    </Action>
    <Method Name="MovePosition" Id="{f39b9c6e-cc98-44eb-b8ce-859f99bbbfd9}">
      <Declaration><![CDATA[
METHOD MovePosition : BOOL
VAR_INPUT
	Position	: UINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF PackState = E_State.Execute THEN
	IF MachineMode = E_MachineMode.Auto THEN
		IF FB_MastAxis.Homed AND FB_LHDAxis.Homed THEN
			IF Position <= 20 THEN
				uiSetPositionID		:= Position;
				MovePosition 		:= TRUE;
			ELSE
				stEvent.diEventID := UDINT_TO_DINT(8);		// Set the error number "Position Not Defined"
				MovePosition 		:= FALSE;
				iAutoState := 1000;		
			END_IF
		ELSE
			stEvent.diEventID := UDINT_TO_DINT(7);		// Set the error number "Not Homed"
			MovePosition 		:= FALSE;
			iAutoState := 1000;		
		END_IF
	ELSE
		stEvent.diEventID := UDINT_TO_DINT(9);		// Set the error number "Not in Auto"
		MovePosition 		:= FALSE;
		iAutoState := 1000;				
	END_IF
ELSE
	stEvent.diEventID := UDINT_TO_DINT(11);		// Set the error number "Not in Execute"
	MovePosition 		:= FALSE;
	iAutoState := 1000;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="Pick" Id="{a4a41601-d2f0-49b0-9655-1a4bd3706b25}">
      <Declaration><![CDATA[
METHOD Pick : BOOL]]></Declaration>
      <Implementation>
        <ST><![CDATA[FB_MastAxis.CurrentPosition;

IF PackState = E_State.Execute THEN
	IF MachineMode = E_MachineMode.Auto THEN
		IF FB_MastAxis.Homed AND FB_LHDAxis.Homed THEN
			IF uiMovePosBaseline = stMessage.uiPositionID THEN
				IF FB_MastAxis.CurrentPosition - 5 < (stXmlLiftData.lrLevel[uiSetPositionID]-stXmlLiftData.lrOffset) AND FB_MastAxis.CurrentPosition + 5 > (stXmlLiftData.lrLevel[uiSetPositionID]-stXmlLiftData.lrOffset) THEN
					Pick		:= TRUE;
				ELSE
					stEvent.diEventID := UDINT_TO_DINT(12);		// Set the error number "Initial Problematic Position"
					Pick		:= FALSE;
					iAutoState	:= 1000;			
				END_IF
			ELSE
				stEvent.diEventID := UDINT_TO_DINT(18);		// Pick Request Incorect Level ID After Move
				Pick		:= FALSE;
				iAutoState	:= 1000;					
			END_IF 
		ELSE
			stEvent.diEventID := UDINT_TO_DINT(7);		// Set the error number "Not Homed"
			Pick			:= FALSE;
			iAutoState		:= 1000;		
		END_IF
	ELSE
		stEvent.diEventID := UDINT_TO_DINT(9);		// Set the error number "Not in Auto"
		Pick				:= FALSE;
		iAutoState			:= 1000;				
	END_IF
ELSE
	stEvent.diEventID := UDINT_TO_DINT(11);		// Set the error number "Not in Execute"
	Pick					:= FALSE;
	iAutoState				:= 1000;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="Place" Id="{bace04b3-1de0-455c-bf3e-f261dc3df432}">
      <Declaration><![CDATA[
METHOD Place : BOOL]]></Declaration>
      <Implementation>
        <ST><![CDATA[FB_MastAxis.CurrentPosition;

IF PackState = E_State.Execute THEN
	IF MachineMode = E_MachineMode.Auto THEN
		IF FB_MastAxis.Homed AND FB_LHDAxis.Homed THEN
			IF uiMovePosBaseline = stMessage.uiPositionID THEN
				IF FB_MastAxis.CurrentPosition - 5 < (stXmlLiftData.lrLevel[uiSetPositionID]+stXmlLiftData.lrOffset) AND FB_MastAxis.CurrentPosition + 5 > (stXmlLiftData.lrLevel[uiSetPositionID]+stXmlLiftData.lrOffset) THEN
					Place		:= TRUE;
				ELSE					
					stEvent.diEventID := UDINT_TO_DINT(12);		// Set the error number "Initial Mast Axis Problematic Position"
					Place		:= FALSE;
					iAutoState	:= 1000;			
				END_IF
			ELSE
				stEvent.diEventID := UDINT_TO_DINT(19);		// Place Request Incorect Level ID After Move
				Place		:= FALSE;
				iAutoState	:= 1000;					
			END_IF 
		ELSE
			stEvent.diEventID := UDINT_TO_DINT(7);		// Set the error number "Not Homed"
			Place			:= FALSE;
			iAutoState		:= 1000;
		END_IF
	ELSE
		stEvent.diEventID := UDINT_TO_DINT(9);		// Set the error number "Not in Auto"
		Place				:= FALSE;
		iAutoState			:= 1000;				
	END_IF
ELSE
	stEvent.diEventID := UDINT_TO_DINT(11);		// Set the error number "Not in Execute"
	Place					:= FALSE;
	iAutoState				:= 1000;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Property Name="PortNumber" Id="{4db1f56d-b267-4f63-9f33-c3036f6899d0}">
      <Declaration><![CDATA[
PROPERTY PortNumber : UINT
]]></Declaration>
      <Get Name="Get" Id="{1513be57-9ec7-4368-84bd-4ea774bc1147}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[PortNumber := uiPortNumber;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{05d8c559-610e-4d7f-94be-18d9461b1148}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[uiPortNumber := PortNumber;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <LineIds Name="FB_Lift">
      <LineId Id="5898" Count="2" />
      <LineId Id="6189" Count="2" />
      <LineId Id="6347" Count="0" />
      <LineId Id="6344" Count="2" />
      <LineId Id="6304" Count="0" />
      <LineId Id="6319" Count="0" />
      <LineId Id="6321" Count="22" />
      <LineId Id="6320" Count="0" />
      <LineId Id="6280" Count="0" />
      <LineId Id="6193" Count="1" />
      <LineId Id="6264" Count="1" />
      <LineId Id="6195" Count="0" />
      <LineId Id="6282" Count="0" />
      <LineId Id="6272" Count="0" />
      <LineId Id="6274" Count="1" />
      <LineId Id="6273" Count="0" />
      <LineId Id="6279" Count="0" />
      <LineId Id="6270" Count="0" />
      <LineId Id="6284" Count="0" />
      <LineId Id="6286" Count="1" />
      <LineId Id="6271" Count="0" />
      <LineId Id="6348" Count="2" />
      <LineId Id="6196" Count="0" />
      <LineId Id="6266" Count="2" />
      <LineId Id="6305" Count="2" />
      <LineId Id="6197" Count="0" />
      <LineId Id="6188" Count="0" />
      <LineId Id="6198" Count="0" />
      <LineId Id="5901" Count="0" />
      <LineId Id="5903" Count="0" />
      <LineId Id="5906" Count="2" />
      <LineId Id="6052" Count="2" />
      <LineId Id="5981" Count="0" />
      <LineId Id="5980" Count="0" />
      <LineId Id="5902" Count="0" />
      <LineId Id="5605" Count="0" />
      <LineId Id="5609" Count="1" />
      <LineId Id="6125" Count="0" />
      <LineId Id="5613" Count="0" />
      <LineId Id="5857" Count="0" />
      <LineId Id="5615" Count="2" />
      <LineId Id="5619" Count="2" />
      <LineId Id="5624" Count="0" />
      <LineId Id="5606" Count="0" />
      <LineId Id="6317" Count="0" />
      <LineId Id="6312" Count="4" />
      <LineId Id="5604" Count="0" />
      <LineId Id="6296" Count="3" />
      <LineId Id="6613" Count="1" />
      <LineId Id="6300" Count="1" />
      <LineId Id="6295" Count="0" />
      <LineId Id="5074" Count="10" />
      <LineId Id="5776" Count="0" />
      <LineId Id="5085" Count="17" />
      <LineId Id="5775" Count="0" />
      <LineId Id="5103" Count="2" />
      <LineId Id="5128" Count="1" />
      <LineId Id="6678" Count="7" />
      <LineId Id="5138" Count="0" />
      <LineId Id="5910" Count="0" />
      <LineId Id="5174" Count="26" />
      <LineId Id="6686" Count="2" />
      <LineId Id="5202" Count="8" />
      <LineId Id="6414" Count="4" />
      <LineId Id="6413" Count="0" />
      <LineId Id="5211" Count="106" />
      <LineId Id="6549" Count="0" />
      <LineId Id="6483" Count="2" />
      <LineId Id="5318" Count="103" />
      <LineId Id="5778" Count="0" />
      <LineId Id="5423" Count="8" />
      <LineId Id="5780" Count="0" />
      <LineId Id="5433" Count="54" />
      <LineId Id="5783" Count="0" />
      <LineId Id="5489" Count="33" />
      <LineId Id="5524" Count="7" />
      <LineId Id="169" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.FB_init">
      <LineId Id="13" Count="7" />
      <LineId Id="10" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.HMI">
      <LineId Id="212" Count="81" />
      <LineId Id="138" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.MovePosition">
      <LineId Id="145" Count="1" />
      <LineId Id="132" Count="0" />
      <LineId Id="121" Count="0" />
      <LineId Id="109" Count="1" />
      <LineId Id="112" Count="0" />
      <LineId Id="150" Count="1" />
      <LineId Id="113" Count="1" />
      <LineId Id="134" Count="2" />
      <LineId Id="147" Count="0" />
      <LineId Id="137" Count="3" />
      <LineId Id="148" Count="0" />
      <LineId Id="141" Count="3" />
      <LineId Id="149" Count="0" />
      <LineId Id="35" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.Pick">
      <LineId Id="152" Count="1" />
      <LineId Id="124" Count="1" />
      <LineId Id="109" Count="0" />
      <LineId Id="155" Count="0" />
      <LineId Id="112" Count="1" />
      <LineId Id="150" Count="1" />
      <LineId Id="161" Count="0" />
      <LineId Id="147" Count="1" />
      <LineId Id="158" Count="1" />
      <LineId Id="162" Count="0" />
      <LineId Id="160" Count="0" />
      <LineId Id="156" Count="0" />
      <LineId Id="131" Count="13" />
      <LineId Id="55" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.Place">
      <LineId Id="176" Count="1" />
      <LineId Id="146" Count="2" />
      <LineId Id="179" Count="0" />
      <LineId Id="149" Count="0" />
      <LineId Id="157" Count="2" />
      <LineId Id="185" Count="0" />
      <LineId Id="160" Count="1" />
      <LineId Id="182" Count="1" />
      <LineId Id="186" Count="0" />
      <LineId Id="184" Count="0" />
      <LineId Id="180" Count="0" />
      <LineId Id="162" Count="13" />
      <LineId Id="4" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.PortNumber.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_Lift.PortNumber.Set">
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>